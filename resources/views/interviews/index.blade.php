@extends('layouts.app')

@section('title', 'Interviews')

@section('content')
<div><a href = "{{url('/interviews/create')}}">Add new interview</a></div>
<h1>List of Interviews</h1>
<!-- class = table (by bootstrap)-->
<table class = "table">
    <tr>
        <th>Id</th><th>Candidate</th><th>Interviewer</th><th>Date</th><th>Description</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>
                @if(isset($interview->candidate_id))
                {{$interview->candidateInInterview->name}}
                @endif
           </td>
           <td>
                @if(isset($interview->user_id))
                {{$interview->user->name}}
                @endif
           </td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->description}}</td>            
        </tr>
    @endforeach
</table>
@endsection
