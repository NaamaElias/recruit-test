@extends('layouts.app')

@section('title', 'Create interview')

@section('content')

<h1>Create Interview</h1>
<form method = "post" action = "{{action('InterviewController@store') }}">
    @csrf <!-- Security -->

    <div class="form-group">
        <label for="candidate_id" class="col-md-1 col-form-label text-right">{{ __('Candidate') }}</label>
        <div class="col-md-6">
            <select class="form-control" name="candidate_id">
                @foreach ($candidates as $candidate)
                <option value="{{$candidate->id}}">
                    {{$candidate->name}}
                </option>
                @endforeach
            </select>
        </div>
    </div>

<!-- try Q3b  -------------------------
    <div class="dropdown">
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        @if(isset($candidate->user_id))
            {{$candidate->owner->name}}
        @else
        Assign Interviewer
        @endif
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    @foreach($users as $user)
        <a class="dropdown-item" href="{{route('interviews.defineuser',[$candidate->id,$user->id])}}">{{$user->name}}</a>
    @endforeach
    </div>
    </div>
    -->




    //Q3
    <div class="form-group">
        <label for="user_id" class="col-md-1 col-form-label text-right">{{ __('Interviewer') }}</label>
        <div class="col-md-6">
            <select class="form-control" name="user_id">
                @foreach ($users as $user)
                <option value="{{$user->id}}">
                    {{$user->name}}
                </option>
                @endforeach
            </select>
        </div>
    </div>

    
    <div class="form-group">
        <label for = "date">Interview Date</label>
        <input type = "text" class="form-control" name = "date"> 
    </div>
    <div class="form-group">
        <label for = "description">Interview Description</label>
        <input type = "text" class="form-control" name = "description"> 
    </div>
    <div>
        <input type = "submit" name = "submit" value = "Create Interview">
    </div>
</form>       
@endsection