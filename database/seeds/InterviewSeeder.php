<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
            'date' => Carbon::now(),
            'description' => 'The candidate was good'
            ],
            [
            'date' => Carbon::now(),
            'description' => 'There is a complete match'
            ],
        ]); 
    }
}
