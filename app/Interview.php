<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    //protected $fillable = ['date','description'];
    public $timestamps = false;


    public function candidateInInterview(){
        return $this->belongsTo('App\Candidate','candidate_id') ;
    }

    public function user(){
        return $this->belongsTo('App\User','user_id') ;
    }
}
