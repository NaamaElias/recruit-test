<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interview;
use App\Candidate;
use App\User;
use Illuminate\Support\Facades\Auth;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = Interview::all();
        $candidates = Candidate::all();
        $users = User::all();
        return view('interviews.index',compact('interviews','candidates','users'));
    }

    // Q4
    public function myInterviews()
    {
        $interviews = Interview::all();
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $interviews->user; 
        $users = User::all();
        $candidates = Candidate::all();
        return view('interviews.index', compact('candidates','users','interviews'));
    }


    // Q3b
    public function defineUser($cid,$uid = null){
        $candidate = Candidate::findOrFail($cid);
        $candidate->user_id = $uid;
        $candidate->save(); 
        return view('interviews.create');
     
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        {
            $candidates = Candidate::all();
            $users = User::all();
            return view('interviews.create',compact('candidates','users'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interview(); 
        $interview->candidate_id = $request->candidate_id;
        $interview->user_id = $request->user_id;
        $interview->date = $request->date; 
        $interview->description = $request->description;
        $interview->save();
        //$interview->create($request->all());
        return redirect('interviews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
